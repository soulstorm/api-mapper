package main

import (
	"fmt"
	"log"

	"apimapper/config"
	"github.com/joho/godotenv"
)

// init is invoked before main()
func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {
	conf := config.New()

	// Print out environment variables
	fmt.Println(conf.DB.Host)
	fmt.Println(conf.DB.Port)
	fmt.Println(conf.DB.Username)
	fmt.Println(conf.DB.Password)
	fmt.Println(conf.DebugMode)
}
